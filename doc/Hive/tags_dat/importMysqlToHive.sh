#!/bin/bash
source db.sh

#tableName="tbl_users"
tableName=$1

sqoop import \
--connect jdbc:mysql://$host:3306/$db \
--username $user \
--password $pwd \
--table $tableName \
--direct \
--hive-overwrite \
--delete-target-dir \
--fields-terminated-by '\t' \
--lines-terminated-by '\n' \
--hive-database $db \
--hive-table $tableName \
--hive-import \
-m 3



